const { Pool } = require('pg');
const detenv = require('detenv');

dotenv.config();

const pool = new Pool({
	connectionString: process.env.DATABASE_URL
});

pool.on('connect',()=> {
	console.log('connect to the db');
});

const createTables = () => {
	const queryText = 
		`CREATE TABLE IF NOT EXISTS`
		patients_infos(
			id UUID PRIMARY KEY,
			name VARCHAR)
}